import UIKit

class DebugViewController: UIViewController {

    // MARK: - IB Outlets
    @IBOutlet weak var textView: UITextView!
    
    // MARK: - Dependencies
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            // Get debug log so far
            self.textView.text = appDelegate.debugLog
            appDelegate.debugDelegate = self
        }
    }
    
    @IBAction func clearTextView() {
        self.textView.text = ""
    }
    
    @IBAction func displayShareSheet() {
        let activityViewController = UIActivityViewController(activityItems: [self.textView.text], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: {})
    }
}

extension DebugViewController: AppDebugDelegate {
    func debugMessage(_ msg: String) {
        self.textView.text = self.textView.text + msg
    }
}

extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}
