import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    var window: UIWindow?

    // Delegates
    weak var debugDelegate: AppDebugDelegate?
    weak var callStatusDelegate: AppCallStatusDelegate?
    
    // Logging variabless
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        return dateFormatter
    }()

    var debugLog: String = ""


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// MARK: - AppDelegate delegate protocols
protocol AppDebugDelegate: class {
    func debugMessage(_ msg: String)
}

protocol AppCallStatusDelegate: class {
    
    /// Communicates if we have a call connected to the Digital Human Service or not
    ///
    /// - Parameter enabled: call enabled true/false
    func callStatus(_ enabled: Bool)
    
    /// Communicates status messages from the Digital Human service to the app
    ///
    /// - Parameter msg: message sent
    func statusMessage(_ msg: String)
}

protocol UneeqWebMessageDelegate: class {
    func onMessage(_ message: String)
}

// MARK: - Receive message from web SDK to the app
extension AppDelegate: UneeqWebMessageDelegate {
    func onMessage(_ message: String) {
        let msg = self.dateFormatter.string(from: Date()) + " \(message)\n"
        self.debugLog = self.debugLog + msg
        self.debugDelegate?.debugMessage(msg)
    }
}

extension AppCallStatusDelegate {
    /// Default implementation to make this optional in delegates
    func statusMessage(_ msg: String) {
        print("default status message call to make this function optional")
    }
}
