//
//  DHWebDelegate.swift
//  DigitalHumanSDK-KitchenSink-Example
//
//  Created by Molty Haque on 30/05/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import AVFoundation
import WebKit

class DHBridge {
    static let TAG = "DHBridge"
    
    var webView: WKWebView?
    
    init(webView: WKWebView) {
        self.webView = webView
    }
    
    func initialiseUneeq(token: String, options: DHOptions) {
        print(DHBridge.TAG + ": About to start running the webview")
        webView?.evaluateJavaScript("init('\(options.url)', '\(options.workspaceId)', '\(token)', '\(options.enableDiagnostics)');", completionHandler: { (response, error) in
            print("finished initialising webview")
        })
    }
    
    func resumeSession() {
        print(DHBridge.TAG + ": resumeSession")
        webView?.evaluateJavaScript("resumeSession();", completionHandler: { (response, error) in
        })
    }
    
    func pauseSession() {
        print(DHBridge.TAG + ": pauseSession")
        webView?.evaluateJavaScript("pauseSession();", completionHandler: { (response, error) in
        })
    }
    
    func endSession() {
        print(DHBridge.TAG + ": endSession")
        webView?.evaluateJavaScript("endSession();", completionHandler: { (response, error) in
        })
    }
    
    func startRecording() {
        print(DHBridge.TAG + ": startRecording")
        webView?.evaluateJavaScript("startRecording();", completionHandler: { (response, error) in
        })
    }
    
    func stopRecording() {
        print(DHBridge.TAG + ": stopRecording")
        webView?.evaluateJavaScript("stopRecording();", completionHandler: { (response, error) in
        })
    }
    
    func callWelcome() {
        print(DHBridge.TAG + ": playWelcomeMessage")
        webView?.evaluateJavaScript("playWelcomeMessage();", completionHandler: { (response, error) in
        })
    }
    
    func sendTranscript(transcript: String) {
        print(DHBridge.TAG + ": sendTranscript(\(transcript))")
        webView?.evaluateJavaScript("sendTranscript('\(transcript)');", completionHandler: { (response, error) in
        })
    }
    
    func gatherWebRtcMetrics() {
        print(DHBridge.TAG + ": gatherWebRtcMetrics()")
        webView?.evaluateJavaScript("gatherWebRtcMetrics();", completionHandler: { (response, error) in
        })
    }
    
    func setMicInput(deviceId: String, completionHandler: ((Any?, Error?) -> Void)? = nil) {
        print(DHBridge.TAG + ": setMic(\(deviceId))")
        webView?.evaluateJavaScript("setMic('\(deviceId)');", completionHandler: completionHandler)
    }
    
    func getAudioOutputs(completionHandler: ((Any?, Error?) -> Void)? = nil) {
        print(DHBridge.TAG + ": getAudioOutputs")
        webView?.evaluateJavaScript("getAudioOutputs();", completionHandler: completionHandler)
    }
    
    func setAudioOutput(deviceId: String) {
        print(DHBridge.TAG + ": setSpeaker(\(deviceId))")
        webView?.evaluateJavaScript("setSpeaker('\(deviceId)');", completionHandler: { (response, error) in
        })
    }
    
    func setCamera(deviceId: String) {
        print(DHBridge.TAG + ": setCamera(\(deviceId))")
        webView?.evaluateJavaScript("setCamera('\(deviceId)');", completionHandler: { (response, error) in
        })
    }
    
    func stopSpeaking(completionHandler: ((Any?, Error?) -> Void)? = nil) {
        print(DHBridge.TAG + ": stopSpeaking")
        webView?.evaluateJavaScript("stopSpeaking();", completionHandler: completionHandler)
    }
}
