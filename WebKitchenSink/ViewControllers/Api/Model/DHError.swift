//
//  DHError.swift
//  DigitalHumanSDK
//

import Foundation

/// Custom Digital Humans error class, subclassed from Error
public struct DHError: Error {
    var errorString: String?
    
    /// Convenience initializer
    ///
    /// - Parameter message: the message associated with the error
    internal init(_ message: String?) {
        self.errorString = message
    }
    
    /// Description for the DH Error
    public var localizedDescription: String {
        return self.errorString ?? "Unknown error"
    }
}
