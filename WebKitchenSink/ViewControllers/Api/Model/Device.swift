//
//  Device.swift
//  DigitalHumanSDK-KitchenSink-Example
//
//  Created by Molty Haque on 6/06/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//
import Foundation

struct Device: Codable {
    let audioInputs: [AudioInput]?
    let audioOutputs: [AudioOutput]?
    let videoInputs: [VideoInput]?
    let uneeqMessageType: String?
    
    init(audioInputs: [AudioInput], audioOutputs: [AudioOutput], videoInputs: [VideoInput], uneeqMessageType: String) {
        self.audioInputs = audioInputs
        self.audioOutputs = audioOutputs
        self.videoInputs = videoInputs
        self.uneeqMessageType = uneeqMessageType
    }
    
    struct AudioInput: Codable {
        let deviceId: String
        let kind: String
        let label: String
        let groupId: String
    }
    
    struct AudioOutput: Codable {
        let deviceId: String
        let kind: String
        let label: String
        let groupId: String
    }
    
    struct VideoInput: Codable {
        let deviceId: String
        let kind: String
        let label: String
        let groupId: String
    }
}
