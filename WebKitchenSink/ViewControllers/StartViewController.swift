import UIKit
import AVFoundation
import WebKit

class StartViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var diagnosticsSwitch: UISwitch!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    var dhBridge: DHBridge?
    // MARK: - State
    var loading: Bool = false {
        didSet {
            if self.loading == true {
                self.activitySpinner.startAnimating()
            } else {
                self.activitySpinner.stopAnimating()
            }
        }
    }
    
    var verbosity: Bool = false {
        didSet {
            self.diagnosticsSwitch.isOn = self.verbosity
        }
    }
    
    // MARK: - View overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Default setting
        self.verbosity = false
        
        // Check permissions
        self.checkPermissions()
    }
    
    // MARK: - Actions
    @IBAction func startSession(_ sender: Any) {
        self.performSegue(withIdentifier: "showCall", sender: self)
    }
    
    @IBAction func cancelSession(_ sender: Any) {
        self.dhBridge?.endSession()
        self.loading = false
    }

    @IBAction func toggleVerbosity(_ sender: Any) {
        self.verbosity.toggle()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showCall") {
            let vc = segue.destination as! CallViewController
            vc.enableDiagnostics = self.diagnosticsSwitch.isOn
       }
    }
}

extension StartViewController: AppCallStatusDelegate {
    func callStatus(_ enabled: Bool) {
        if enabled == true {
            self.performSegue(withIdentifier: "showCall", sender: self)
            self.loading = false
            self.statusLabel.text = ""
        } else {
            print("Call not started")
            self.loading = false
        }
    }
    
    func statusMessage(_ msg: String) {
        self.statusLabel.text = msg
    }
}

// MARK: - Permissions checking
extension StartViewController {
    func checkPermissions() {
        // Microphone permission
        if AVAudioSession.sharedInstance().recordPermission == .undetermined {
            // Request permission
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
        }
        
        // Camera permission
        if AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            })
        }
    }
}
