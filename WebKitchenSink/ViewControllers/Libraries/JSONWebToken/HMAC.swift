import Foundation

internal enum HMACAlgorithm {
  case sha256
  case sha384
  case sha512
}
