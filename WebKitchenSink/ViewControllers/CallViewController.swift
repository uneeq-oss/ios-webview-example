import Foundation
import UIKit
import AVFoundation
import WebKit
import SwiftyJSON

class CallViewController: UIViewController, WKNavigationDelegate {
    //Base server you want your application to point to
    static let BASE_URL = "https://api.au.uneeq.io"

    //API path to get the Single Use Token required to start the session with UneeQ
    static let API_PATH = "/api/v1/clients/access/tokens/"

    //The Workspace ID that UneeQ will have issued you
    static let WORKSPACE_ID = "2f59e32b-78cc-4759-a0bb-7f7900148eea"
    
    //JWT token
    static let CLIENT_SECRET = "f77f15b8-c6f0-454x-y47x-ccd11ba798b0"
    static let APP_SESSION_ID = "IOS"

    // MARK: - Outlets
    @IBOutlet weak var replyLabel: UILabel!
    @IBOutlet weak var remoteContainer: UIView!
    
    @IBOutlet weak var pauseResumeButton: UIButton!
    @IBOutlet weak var pushToSpeakButton: UIButton!
    var dhBridge: DHBridge?
    var device: Device?
    var chosenVideoInput: Device.VideoInput?
    var webMessageDelegate: UneeqWebMessageDelegate?
    var enableDiagnostics = false
    // MARK: - View state properties
    var speakerOn: Bool = true {
        didSet {
            switch self.speakerOn {
            case false:
                self.setAudioOutputSpeaker(enabled: false)
            case true:
                self.setAudioOutputSpeaker(enabled: true)
            }
        }
    }
    
    var pauseSession: Bool = false {
        didSet {
            switch self.pauseSession {
            case false:
                self.dhBridge?.resumeSession()
                self.pauseResumeButton.setTitle("Pause Session", for: .normal)
            case true:
                self.dhBridge?.pauseSession()
                self.pauseResumeButton.setTitle("Resume Session", for: .normal)
            }
        }
    }
    
    // MARK: - View overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            self.webMessageDelegate = appDelegate
        }
        
        self.pauseResumeButton.addTarget(self, action: #selector(self.pauseResumeDidTap), for: .touchUpInside)
        
        // Configure initial state
        //self.speakerOn = true
        
        self.addRemotePreviewView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.navigationController?.topViewController != self {
            self.dhBridge?.endSession()
        }
    }
    
    /// Adds the remote preview view to the screen, received from the DigitalHumanSDK
    func addRemotePreviewView() {
        if let url = Bundle.main.url(forResource: "index", withExtension: "html") {
            let request = URLRequest(url: url)
            let rect = CGRect(x: 0, y: 0, width: remoteContainer.frame.width, height: remoteContainer.frame.height)
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true
            let configuration = WKWebViewConfiguration()
            configuration.allowsInlineMediaPlayback = true
            configuration.mediaTypesRequiringUserActionForPlayback = []
            configuration.preferences = preferences
            configuration.userContentController.add(self, name: "onMessage")
            let webView = WKWebView(frame: rect, configuration: configuration)
            webView.navigationDelegate = self
            dhBridge = DHBridge(webView: webView)
            remoteContainer.addSubview(webView)
//            let showcaseUrl = URL(string: "https://creator.au.uneeq.io/try/cd005fe1-33f0-4661-8011-e2097b70ae34")!
//            var urlRequest:URLRequest = URLRequest(url: showcaseUrl)
            webView.load(request)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let url = webView.url?.absoluteString {
            print("url \(url)")
            if (url.hasSuffix("index.html")) {
                let options = DHOptions(workspaceId: CallViewController.WORKSPACE_ID, clientSecret: CallViewController.CLIENT_SECRET, url: CallViewController.BASE_URL, enableDiagnostics: enableDiagnostics)
                DHTokenManager().retrieveToken(options: options, sessionId: CallViewController.APP_SESSION_ID) { (singleUseToken, error) in

                    if let error = error {
                        print("Error retrieving token: \(error)")
                    }

                    guard let singleUseToken = singleUseToken else { return }

                    guard let token = singleUseToken.token else { return }

                    self.dhBridge?.initialiseUneeq(token: token, options: options)
                }
            }
        }
    }

    /// Sets the camera for the local preview view
    func addLocalPreviewView() {
        //set chosen camera first in list
        self.chosenVideoInput = self.device?.videoInputs?[0]
        if (self.chosenVideoInput != nil) {
            //front camera set by default, on simulator setting front camera function while avatar is loading shows black screen instead of avatar
            //self.dhBridge?.setCamera(deviceId: self.chosenVideoInput!.deviceId)
        }
    }
    
    // MARK: - Actions
    @IBAction func optionsDidTap(_ sender: UIButton) {
        self.showOptions()
    }
    
    @IBAction func audioButtonOn(_ sender: UIButton) {
        self.dhBridge?.startRecording()
    }
    
    @IBAction func audioButtonOff(_ sender: UIButton) {
        self.dhBridge?.stopRecording()
    }
    
    @IBAction func pauseResumeDidTap() {
        self.pauseSession.toggle()
    }
    
    /// - Parameter text: avatar text
    func digitalHumanTextReceived(_ text: String) {
        self.replyLabel.text = text
    }
}


extension CallViewController {
    func showOptions() {
        let alertController = UIAlertController(title: "Test options", message: nil, preferredStyle: .actionSheet)
        
        let welcomeAction = UIAlertAction(title: "Send welcome", style: .default) { [weak self] (action) in
            self?.dhBridge?.callWelcome()
        }

        let speakerAction = UIAlertAction(title: "Turn speakers " + self.textForBoolean(self.speakerOn), style: .default) { [weak self] (action) in
            self?.speakerOn.toggle()
        }
        
        //get video input from web sdk
        let listAvailableCameraOutputs =  UIAlertAction(title: "List Camera outputs", style: .default) { [weak self] (action) in
            self?.presentSelectCameraOutput()
        }
        
        let sendTranscriptAction = UIAlertAction(title: "Send transcript", style: .default) { [weak self] (action) in
            self?.presentSendTranscript()
        }
        
        let listAvailableInputs =  UIAlertAction(title: "List audio inputs", style: .default) { [weak self] (action) in
            self?.presentSelectMicInputs()
        }
        
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        alertController.addAction(welcomeAction)
        alertController.addAction(speakerAction)
        alertController.addAction(listAvailableCameraOutputs)
        alertController.addAction(sendTranscriptAction)
        alertController.addAction(listAvailableInputs)

        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textForBoolean(_ bool: Bool) -> String {
        if bool == false {
            return "on"
        } else {
            return "off"
        }
    }
}

// MARK: - Direct text sending to the SDK
extension CallViewController {
    func presentSendTranscript() {
        let alertController = UIAlertController(title: "Enter text to send", message: nil, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter text here"
        }
        
        
        let sendAction = UIAlertAction(title: "Send", style: .default) { [weak self] action in
            if let text = alertController.textFields?.first?.text {
                self?.dhBridge?.sendTranscript(transcript: text)
            }
        }
        
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(sendAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Camera functions
extension CallViewController {
    
    func presentSelectCameraOutput() {
        
        let alertController = UIAlertController(title: "Select camera", message: nil, preferredStyle: .alert)
        
        self.device?.videoInputs?.forEach({ input in
            var title: String = input.label
            if input.deviceId == self.chosenVideoInput?.deviceId {
                title = title + " (selected)"
            }
            
            let action = UIAlertAction(title: title, style: .default, handler: { [weak self] action in
                self?.chosenVideoInput = input
                self?.dhBridge?.setCamera(deviceId: input.deviceId)
            })
            
            alertController.addAction(action)
        })
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Microphone input selection
extension CallViewController {
    func presentSelectMicInputs() {
        let currentInput = self.device?.audioInputs![0] //JS SDK does not support getting current audio input
        
        let alertController = UIAlertController(title: "Select input", message: nil, preferredStyle: .alert)
        
        self.device?.audioInputs?.forEach({ input in
            var title: String = input.label
            if input.deviceId == currentInput?.deviceId {
                title = title + " (selected)"
            }
            
            let action = UIAlertAction(title: title, style: .default, handler: { [weak self] action in
                self?.dhBridge?.setMicInput(deviceId: input.deviceId)
            })
            
            alertController.addAction(action)
        })
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Audio output selection
extension CallViewController {
    
    //MARK:- Manual Speaker Enable and Disable
    func setAudioOutputSpeaker(enabled: Bool)
    {
        let session = AVAudioSession.sharedInstance()
        var _: Error?
        try? session.setCategory(AVAudioSession.Category.playAndRecord)
        try? session.setMode(AVAudioSession.Mode.voiceChat)
        if enabled {
            try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } else {
            try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        }
        try? session.setActive(true)
    }
}

extension CallViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "onMessage" {
            print(message.body)
            let json = JSON(message.body)
            self.webMessageDelegate?.onMessage(json.rawString(String.Encoding.utf8, options: []) ?? "")
            let dict = convertStringToDictionary(text: json["jsonString"].stringValue)
            let uneeqMessageType = dict!["uneeqMessageType"]! as! String
            switch uneeqMessageType {
            case "DeviceListUpdated":
                let devicesDict = dict!["devices"] as! [String : Any?]
                
                var audioInputs = [Device.AudioInput]()
                var audioOutputs = [Device.AudioOutput]()
                var videoInputs = [Device.VideoInput]()
                
                let jsonAudioInputs = JSON((devicesDict as? NSDictionary)?.object(forKey: "audioInput")!)
                if (jsonAudioInputs != JSON.null || jsonAudioInputs.array != nil) {
                    
                    for item in jsonAudioInputs.array! {
                        let deviceId = item["deviceId"].stringValue
                        let kind = item["kind"].stringValue
                        let label = item["label"].stringValue
                        let groupId = item["groupId"].stringValue
                        audioInputs.append(Device.AudioInput(deviceId: deviceId, kind: kind, label: label, groupId: groupId))
                    }
                }
                
                let jsonAudioOutputs = JSON((devicesDict as? NSDictionary)?.object(forKey: "audioOutput")!)
                if (jsonAudioOutputs != JSON.null || jsonAudioOutputs.array != nil) {
                    
                    for item in jsonAudioOutputs.array! {
                        let deviceId = item["deviceId"].stringValue
                        let kind = item["kind"].stringValue
                        let label = item["label"].stringValue
                        let groupId = item["groupId"].stringValue
                        audioOutputs.append(Device.AudioOutput(deviceId: deviceId, kind: kind, label: label, groupId: groupId))
                    }
                }
                
                let jsonVideoInputs = JSON((devicesDict as? NSDictionary)?.object(forKey: "videoInput")!)
                if (jsonVideoInputs != JSON.null || jsonVideoInputs.array != nil) {
                    
                    for item in jsonVideoInputs.array! {
                        let deviceId = item["deviceId"].stringValue
                        let kind = item["kind"].stringValue
                        let label = item["label"].stringValue
                        let groupId = item["groupId"].stringValue
                        videoInputs.append(Device.VideoInput(deviceId: deviceId, kind: kind, label: label, groupId: groupId))
                    }
                }
                self.device = Device(audioInputs: audioInputs, audioOutputs: audioOutputs, videoInputs: videoInputs, uneeqMessageType: "DeviceListUpdated")
                self.addLocalPreviewView()
                break
            case "AvatarAnswer":
                let answerSpeech = dict!["answerSpeech"] as? String
                if (answerSpeech != nil) {
                    digitalHumanTextReceived(answerSpeech!)
                }
                break
            default:
                break
            }
        }
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
}
