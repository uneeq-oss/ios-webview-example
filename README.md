# UneeQ Javascript (JS) SDK for iOS

## Overview
The Javascript (JS) SDK for iOS is a library that can be embedded into a webview allowing the UneeQ Digital Human platform integration with your iOS apps. The preferred approach is to load a local html file which iniliases the JS sdk displayed in a webview.

### Installation

#### Example project

This is a "kitchen sink" demonstration of the JS SDK integration into an iOS app.
#### Requirements
Required minimum iOS 14.5 and Xcode 12.5 with this JS SDK. Whilst the JS SDK can be called by Objective C, all examples are Swift based.

### 1. Adding the JS SDK to your code
Under the project directory create a folder named “Web”. In the folder add the Uneeq.js file (contact UneeQ support for this), then add an index.html file (copy from example project). 

This will initialise uneeq.js and display the avatar and camera video in its div containers. This will also act as an interface of the JS SDK to native swift code.

### 2. Add webview to remote container
Replace the current avatar container view implementation with the webview which will load the index.html file:

```
  func addRemotePreviewView() {
        if let url = Bundle.main.url(forResource: "index", withExtension: "html") {
            let request = URLRequest(url: url)
            let rect = CGRect(x: 0, y: 0, width: remoteContainer.frame.width, height: remoteContainer.frame.height)
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true
            let configuration = WKWebViewConfiguration()
            configuration.allowsInlineMediaPlayback = true
            configuration.mediaTypesRequiringUserActionForPlayback = []
            configuration.preferences = preferences
            configuration.userContentController.add(self, name: "onMessage")
            let webView = WKWebView(frame: rect, configuration: configuration)
            webView.navigationDelegate = self
            dhBridge = DHBridge(webView: webView)
            remoteContainer.addSubview(webView)
            webView.load(request)
        }
    }
```


Consequently, the view controller must implement WKNavigationDelegate.

### 3. Add JS callback function
In the same view controller add callback function by implementing WKScriptMessageHandler:

```
extension CallViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "onMessage" {
            print(message.body)
            //see kitchen sink app for example way of extracting json responses
        }
    }
```


All js responses will come through this function in json format.

### 4. Add Swift-JS Bridge
Create a bridge.swift file, add init function passing the webview (loaded with the index.html):
```
class DHBridge {
     var webView: WKWebView?
     init(webView: WkWebView) {
          self.webView = webView
     }
```


### 5. Initialise UneeQ JS SDK
In Bridge.swift file, add init function:
```
func initialiseUneeq(token: String, options: DHOptions) {
   webView?.evaluateJavaScript("init('\(options.url)', '\(options.workspaceId)', '\(token)', '\(options.enableDiagnostics)');", completionHandler: { (response, error) in 
print("finished initialising webview") 
})
}
```

After initialisation, the uneeqMessageType: “DeviceListUpdated'' json response will be sent from JS SDK which has the device audio/video information, this info should be saved in order to use for setting audio/video via deviceId later. See kitchenSink app for example implementation.

Example response:

```
{
 "devices": {
   "audioInput": [
     {
       "deviceId": "BD9EBE26676E2A56AE54AD4442F5FA8CAB42DDAF",
       "kind": "audioinput",
       "label": "iPhone Microphone",
       "groupId": ""
     }
   ],
   "audioOutput": [],
   "videoInput": [
     {
       "deviceId": "C0DEF96CDC27F3E9A0F4D683052D7EFFC6201AB",
       "kind": "videoinput",
       "label": "Front Camera",
       "groupId": ""
     },
     {
       "deviceId": "AB6BB4803E4A9181C2EDF41BC3CB3E5950B",
       "kind": "videoinput",
       "label": "Back Camera",
       "groupId": ""
     }
   ]
 },
 "uneeqMessageType": "DeviceListUpdated"
}
```

### 6. Functionality & method names
In Bridge.swift file, add functions which propagates to JS function with “webView.evaluateJavaScript(<js function>)”

Please see UneeQ SDK docs - https://docs.uneeq.io/#/js_sdk?id=the-uneeq-js-sdk

### 7. Install pods
Pod install in command line:

```
pod 'DigitalHumanSDK', :git => 'https://gitlab.com/uneeq-oss/ios-sdk.git'
pod ‘GoogleWebRTC’
pod ‘StompClientLib’
```

### 8. Remove all references to 'DigitalHumanSDK'

## Support

If you require further support please contact your UneeQ customer service representative or email help@uneeq.com.
