//
//  DHTokenManager.swift
//  DigitalHumanSDK-KitchenSink-Example
//
//  Created by Molty Haque on 2/06/21.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation

internal typealias DHTokenManagerCompletion = (SingleUseToken?, DHError?) -> Void

internal class DHTokenManager {
    internal lazy var sessionManager: DHSessionManagerProtocol = {
        return DHSessionManager()
    }()

    func retrieveToken(options: DHOptions, sessionId: String, completion: @escaping DHTokenManagerCompletion) {
        guard let clientSecret = options.clientSecret else {
            completion(nil, DHError("No client secret supplied"))
            return
        }
        
         let data = TokenRequest(sid: sessionId,
                                          workspace: options.workspaceId,
                                          customData: options.customData)
         let request = APIEndpoint.getSingleUseToken(jwtSecret: clientSecret, data: data)
         
         self.sessionManager.request(request, completion: { (response) in
             let jsonDecoder = JSONDecoder()

             if let data = response.data, let singleUseToken = try? jsonDecoder.decode(SingleUseToken.self, from: data) {
                
                completion(singleUseToken, nil)
             } else {
                completion(nil, DHError("Error configuring session"))
             }
         })
    }
}
