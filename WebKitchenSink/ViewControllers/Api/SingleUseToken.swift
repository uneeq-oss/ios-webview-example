
//
//  SingleUseToken.swift
//  DigitalHumanSDK
//

import Foundation

internal struct SingleUseToken: Codable {
    let token: String?
}
