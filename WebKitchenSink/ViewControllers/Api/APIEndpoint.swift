    //
//  APIEndpoints.swift
//  DigitalHumanSDK
//

import Foundation
import Alamofire

 struct APIBaseURLInfo {
    static var apiURL = URL(string: "https://api.au.uneeq.io")!
}
    
    enum APIEndpoint: URLConvertible, URLRequestConvertible {
        
    case getSingleUseToken(jwtSecret: String, data: TokenRequest)

     var baseURL: URL {
        return APIBaseURLInfo.apiURL
    }
    
      var path: String {
        switch self {
            case .getSingleUseToken: return "/api/v1/clients/access/tokens"
        }
    }
    
     var parameters: [String: String] {
        switch self {
        case .getSingleUseToken(_, let data):
            var dictionaryToReturn: [String: String] = ["sid": data.sid, "fm-workspace": data.workspace]
            
            if let customData = data.customData {
                let customDataString = "{" + customData.map { (key: String, value: String) -> String in
                    "\"\(key)\" : \"\(value)\""
                }.joined(separator: ",") + "}"
                
                dictionaryToReturn["fm-custom-data"] = customDataString
            }
            
            return dictionaryToReturn
        }
    }
    
      var httpMethod: Alamofire.HTTPMethod {
        return .post
    }
    
     var needAuth: Bool {
        switch self {
        default:
            return false
        }
    }
    
      var parameterEncoding: Alamofire.ParameterEncoding {
        switch self {
        case .getSingleUseToken:
            return URLEncoding.httpBody
        default:
            return URLEncoding.queryString
        }
    }
    
    func asURL() throws -> URL {
        return self.baseURL.appendingPathComponent(self.path)
    }
    
     func asURLRequest() throws -> URLRequest {
        var request = URLRequest(url: try self.asURL())
        request.httpMethod = self.httpMethod.rawValue

        switch self {
        case .getSingleUseToken(let jwtSecret, let data):
            if let encodedString = self.jwtEncodeDictionary(self.parameters, jwtSecret: jwtSecret) {
                request.httpBody = encodedString.data(using: .utf8)
            }
            request.addValue("application/jwt", forHTTPHeaderField: "content-type")
            request.addValue(data.workspace, forHTTPHeaderField: "workspace")
        }
        return request
    }
    
}

// MARK: - JWT functions
extension APIEndpoint {
    func jwtEncodeDictionary(_ dictionary: [String: String], jwtSecret: String) -> String? {
        guard
            let secretData = jwtSecret.data(using: .utf8)
        else {
            return nil
        }
        
        let encodedString = JWTencode(.hs256(secretData)) { builder in
            for parameter in dictionary {
                builder[parameter.key] = parameter.value
            }
        }
        
        return encodedString
    }
}
