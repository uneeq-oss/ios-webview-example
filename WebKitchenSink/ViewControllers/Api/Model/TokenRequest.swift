import Foundation

internal struct TokenRequest: Codable {
    var sid: String
    var workspace: String
    var customData: [String: String]?
}

