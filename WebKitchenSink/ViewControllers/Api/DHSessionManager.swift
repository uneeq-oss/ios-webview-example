//
//  DHSessionManager.swift
//  DigitalHumanSDK
//

import Foundation
import Alamofire

internal typealias DHSessionResponse = (DataResponse<String>) -> Void

internal protocol DHSessionManagerProtocol {
    func request(_ request: APIEndpoint, completion: DHSessionResponse?)
}

internal class DHSessionManager: DHSessionManagerProtocol {
    lazy var sessionManager: SessionManager = {
        return Alamofire.SessionManager.default
    }()
    
    func request(_ request: APIEndpoint, completion: DHSessionResponse?) {
        if let completion = completion {
            self.sessionManager.request(request).responseString(completionHandler: completion)
        } else {
            self.sessionManager.request(request)
        }
    }
}




