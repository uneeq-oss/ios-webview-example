//
//  DHOptions.swift
//  DigitalHumanSDK
//

import Foundation

/// Options for configuring a DigitalHumanSDK session with a client secret
public struct DHOptions {
    let workspaceId: String
    let clientSecret: String?
    let url: String
    let customData: [String: String]?
    let sendLocalVideo: Bool
    let vadEnabled: Bool
    let enableDiagnostics: Bool
    
    /// Initializer for all options to configure a DigitalHumanSDK session
    ///
    /// - Parameters:
    ///   - workspaceId: Your Digital Humans service Workspace ID
    ///   - clientSecret: Your Digital Humans service client secret
    ///   - url: The base digital humans API URL
    ///   - customData: Optional, custom payload for submitting with session request
    ///   - sendLocalVideo: Optional, false disables camera access or camera accessing calls
    ///   - vadEnabled: Optional, true enables Voice-Activity-Detection, false leaves Voice-Activity-Detection disabled. Default is false
    public init(workspaceId: String,
                clientSecret: String,
                url: String,
                enableDiagnostics: Bool = false,
                customData: [String: String]? = nil,
                sendLocalVideo: Bool = true,
                vadEnabled: Bool = false) {
        self.workspaceId = workspaceId
        self.clientSecret = clientSecret
        self.url = url
        self.customData = customData
        self.sendLocalVideo = sendLocalVideo
        self.vadEnabled = vadEnabled
        self.enableDiagnostics = enableDiagnostics
    }
}

/// Options for configuring a DigitalHumanSDK session with a tokenId
public struct DHTokenOptions {
    let workspaceId: String
    let tokenId: String
    let url: String
    let customData: [String: String]?
    let sendLocalVideo: Bool
    let vadEnabled: Bool
    let enableDiagnostics: Bool

    /// Initializer for all options to configure a DigitalHumanSDK session
    ///
    /// - Parameters:
    ///   - workspaceId: Your Digital Humans service Workspace ID
    ///   - tokenId: Your single use token for initialising the service
    ///   - url: The base digital humans API URL
    ///   - customData: Optional, custom payload for submitting with session request
    ///   - sendLocalVideo: Optional, false disables camera access or camera accessing calls
    ///   - vadEnabled: Optional, true enabled enables Voice-Activity-Detection, false is  Push-To-Talk. Default is PTT
    public init(workspaceId: String,
                tokenId: String,
                url: String,
                customData: [String: String]? = nil,
                sendLocalVideo: Bool = true,
                vadEnabled: Bool = false,
                enableDiagnostics: Bool = false) {
        self.workspaceId = workspaceId
        self.tokenId = tokenId
        self.url = url
        self.customData = customData
        self.sendLocalVideo = sendLocalVideo
        self.vadEnabled = vadEnabled
        self.enableDiagnostics = enableDiagnostics
    }
    
    internal func createDHOptions() -> DHOptions {
        return DHOptions(workspaceId: self.workspaceId,
                         clientSecret: "",
                         url: self.url,
                         enableDiagnostics: self.enableDiagnostics, customData: self.customData,
                         sendLocalVideo: self.sendLocalVideo,
                         vadEnabled: self.vadEnabled)
    }
}
